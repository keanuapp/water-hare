import 'package:fluffychat/main.dart' as app;
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

void main() {
  group('screenshots', () {
    final binding = IntegrationTestWidgetsFlutterBinding.ensureInitialized();
    binding.framePolicy = LiveTestWidgetsFlutterBindingFramePolicy.fullyLive;

    testWidgets('startup screenshots', (WidgetTester tester) async {
      app.main();

      // takeScreenshot has been broken for over a year in flutter; see:
      // https://github.com/flutter/flutter/issues/91668
      // We implement a workaround in AppDelegate.swift.
      await binding.takeScreenshot('01-splash');
      await tester.pumpAndSettle(const Duration(milliseconds: 2000));
      await tester.pumpAndSettle(const Duration(milliseconds: 1000));
      await binding.takeScreenshot('02-landing');
      final Finder start = find.widgetWithText(
          MouseRegion, "Let's start",); // EN-only; TODO: l10n.
      await tester.tap(start);

      // Second pumpAndSettle is required to get to next screen.
      await tester.pumpAndSettle(const Duration(milliseconds: 1000));
      await tester.pumpAndSettle(const Duration(milliseconds: 1000));
      await binding.takeScreenshot('03-login');
    });
  });
}
