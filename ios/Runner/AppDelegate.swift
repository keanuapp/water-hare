import UIKit
import Flutter
import integration_test

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
    }

    // Support flutter integration_tests.
    // Next 2 lines per https://github.com/flutter/flutter/issues/91668#issuecomment-1363795472
    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController  
    IntegrationTestPlugin.instance().setupChannels(controller.binaryMessenger)
    
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
