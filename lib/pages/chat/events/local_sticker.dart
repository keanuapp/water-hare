import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:fluffychat/pages/chat/events/sticker_bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';

import '../../../config/app_config.dart';
import '../../sticker/local_sticker.dart';

class LocalStickerView extends StatefulWidget {
  final Event event;
  final LocalSticker sticker;
  final double width = 200;

  const LocalStickerView(this.event, this.sticker, {Key? key})
      : super(key: key);

  @override
  LocalStickerViewState createState() => LocalStickerViewState();
}

class LocalStickerViewState extends State<LocalStickerView> {
  bool? animated;

  @override
  Widget build(BuildContext context) {
    return StickerBubble(
      widget.event,
      width: widget.width,
      height: widget.width,
      backgroundColor: Colors.transparent,
      fit: BoxFit.contain,
      onTap: () {
        setState(() => animated = true);
        showOkAlertDialog(
          context: context,
          message: widget.event.body,
          okLabel: L10n.of(context)!.ok,
        );
      },
      animated: animated ?? AppConfig.autoplayImages,
      imageUrl: widget.sticker.emoticon,
    );
  }
}
