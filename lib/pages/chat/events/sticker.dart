import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:fluffychat/pages/chat/events/image_bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';

import '../../../config/app_config.dart';

class Sticker extends StatefulWidget {
  final Event event;
  final double width = 200;

  const Sticker(this.event, {Key? key}) : super(key: key);

  @override
  StickerState createState() => StickerState();
}

class StickerState extends State<Sticker> {
  bool? animated;

  @override
  Widget build(BuildContext context) {
    return ImageBubble(widget.event,
        width: widget.width,
        height: widget.width,
        backgroundColor: Colors.white,
        fit: BoxFit.contain, onTap: () {
      setState(() => animated = true);
      showOkAlertDialog(
        context: context,
        message: widget.event.body,
        okLabel: L10n.of(context)!.ok,
      );
    }, animated: animated ?? AppConfig.autoplayImages,);
  }
}
