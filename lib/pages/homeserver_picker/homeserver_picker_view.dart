import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import 'homeserver_picker.dart';

class HomeserverPickerView extends StatelessWidget {
  final HomeserverPickerController controller;

  const HomeserverPickerView(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('assets/info-logo.png', width: 300, height: 300),
              const SizedBox(width: double.infinity, height: 20),
              Hero(
                tag: 'loginButton',
                child: Container(
                  padding: const EdgeInsets.all(12),
                  width: double.infinity,
                  height: 80,
                  child: ElevatedButton.icon(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Theme.of(context).colorScheme.primary,
                      foregroundColor:
                          Theme.of(context).colorScheme.primaryContainer,
                    ),
                    onPressed: controller.isLoading
                        ? null
                        : controller.login,
                    icon: const Icon(Icons.start_outlined),
                    label: controller.isLoading
                        ? const LinearProgressIndicator()
                        : Text(L10n.of(context)!.letsStart),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
