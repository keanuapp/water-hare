class LocalSticker {
  String name = "";
  String category = "";
  String emoticon = "";
  String url = "";

  LocalSticker(this.name, this.category, this.emoticon, this.url);

  factory LocalSticker.fromJson(dynamic json) {
    return LocalSticker(json['name'] as String, json['category'] as String,
        json['emoticon'] as String, json['url'] as String,);
  }

  @override
  String toString() {
    return '{ $name, $category,$emoticon,$url }';
  }
}
