import 'package:fluffychat/pages/sticker/local_sticker.dart';

class StickerGroup {
  String category = "";
  List<LocalSticker> stickers = [];

  StickerGroup(this.category, this.stickers);

  factory StickerGroup.fromJson(dynamic json) {
    final category = json['category'];
    final stickersJson = json['stickers'] as List;
    final List<LocalSticker> stickers = stickersJson
        .map((stickerJson) => LocalSticker.fromJson(stickerJson))
        .toList();
    return StickerGroup(category, stickers);
  }

  @override
  String toString() {
    return '{ $category, $stickers}';
  }
}
