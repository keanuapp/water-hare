import 'dart:convert';

import 'package:fluffychat/pages/sticker/local_sticker.dart';
import 'package:fluffychat/pages/sticker/sticker_groups.dart';
import 'package:flutter/cupertino.dart';

class StickerProvider {
  Future<Map<String, List<LocalSticker>>> getStickersWithGroup(
      BuildContext context,) async {
    final stickerGroupsJson = await DefaultAssetBundle.of(context)
        .loadString('assets/stickers/stickers.json');
    final StickerGroups stickerGroups =
        StickerGroups.fromJson(jsonDecode(stickerGroupsJson));
    final stickerGroupMap = {
      for (var group in stickerGroups.packs) group.category: group.stickers
    };
    return stickerGroupMap;
  }

 Map<String, LocalSticker> getStickers(Map<String, List<LocalSticker>> stickerGroupMap) {
    final stickersMap = <String, LocalSticker>{};
    for (final group in stickerGroupMap.values) {
      for (final sticker in group) {
        stickersMap[sticker.url] = sticker;
      }
    }
    return stickersMap;
  }
}
